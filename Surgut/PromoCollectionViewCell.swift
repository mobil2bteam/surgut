
import UIKit
import AlamofireImage

class PromoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var clubImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imageShadowView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.2
        shadowView.layer.shadowOffset = CGSize(width: -1, height: -1)
        shadowView.layer.shadowRadius = 2
    }
    
    func configure(for promo: PromoModel) {
        nameLabel.text = promo.name
        descriptionLabel.text = promo.descriptionShort
        if promo.image != nil {
            if let url = URL(string: promo.image!.url) {
                promoImageView.af_setImage(withURL: url)
            }
        }
        if promo.club.logo != nil {
            if let url = URL(string: promo.club.logo!.url) {
                clubImageView.af_setImage(withURL: url)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clubImageView.image = nil
        promoImageView.image = nil
    }

}
