
import UIKit
import GoogleMaps

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!

    var club: ClubModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(club.lat!), longitude: CLLocationDegrees(club.lng!), zoom: 13)
        self.mapView.camera = camera
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(club.lat!), longitude: CLLocationDegrees(club.lng!))
        marker.title = club.name
        marker.snippet = ""
        marker.setMarkerIcon(link: club.logo!.url)
        marker.map = mapView
    }

}
