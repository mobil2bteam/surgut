
import UIKit

class SocialViewController: UIViewController {

    @IBOutlet weak var socialCollectionView: UICollectionView!
    @IBOutlet weak var socialCollectionViewHeightConstraint: NSLayoutConstraint!

    let observationKey = "socialCollectionView.contentSize"
    var source: [[String: String]] = []
    var changedHeightCallback: ((CGFloat) -> Void)? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
    }

    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }
    
    // MARK: Observation
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            let height = socialCollectionView.collectionViewLayout.collectionViewContentSize.height
            socialCollectionViewHeightConstraint.constant = height
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
            if changedHeightCallback != nil {
                changedHeightCallback!(height + 77.0 + 9.0)
            }
        }
    }

}

extension SocialViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return source.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SocialCollectionViewCell
        let image = UIImage.init(named: source[indexPath.item]["image"]!)
        cell.socialButton.setImage(image, for: .normal)
        cell.url = source[indexPath.item]["url"]!
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalItems = source.count
        let currentItem = indexPath.item + 1
        if totalItems < 3 || currentItem > totalItems - (totalItems % 3) {
            let oldWidth = (collectionView.frame.size.width) / 3
            let itemsInLastRow = Int(totalItems) % Int(3)
            let newWidth = (oldWidth * 3) / CGFloat(itemsInLastRow)
            return CGSize(width: newWidth, height: 50)
        }
        let width = (collectionView.frame.size.width) / 3
        return CGSize(width: width, height: 50)
    }
    /*
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? SocialCollectionViewCell {
                cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? SocialCollectionViewCell {
                cell.contentView.backgroundColor = .white
            }
        }
    }
*/
}
