
import Foundation
import RealmSwift

class FavoriteManager {
    
    static let favoriteClubsKey = "FavoriteManager.clubs"
    
    class func addClub(club: ClubModel) {
        let c = ClubRealmModel(club: club)
        try! uiRealm.write{
            uiRealm.add(c)
        }
    }
    
    class func removeClub(club: ClubModel) {
        let clubs = getFilteredClubs()
        var searchedClub: ClubRealmModel? = nil
        for c in clubs {
            if c.id == club.id {
                searchedClub = c
            }
        }
        if searchedClub != nil {
            try! uiRealm.write{
                uiRealm.delete(searchedClub!)
            }
        }
    }

    class func removeRealmClub(club: ClubRealmModel) {
        try! uiRealm.write{
            uiRealm.delete(club)
        }
    }

    class func getFilteredClubs() -> Results<ClubRealmModel> {
        let array = uiRealm.objects(ClubRealmModel.self)
        return array
    }
    
    class func isClubExist(club: ClubModel) -> Bool {
        let clubs = getFilteredClubs()
        for c in clubs {
            if c.id == club.id {
                return true
            }
        }
        return false
    }
    
}
