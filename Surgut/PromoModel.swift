
import ObjectMapper


class PromoModel: Mappable {
    var id: Int!
    var name: String!
    var image: ImageModel?
    var club: ClubModel!
    var descriptionShort: String! = ""
    var descriptionDetail: String! = ""
    var date: String! = ""
    var dateFrom: String! = ""
    var dateTo: String! = ""
    var unlimited: Bool!
    var code: String = ""

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                                      <- map["id"]
        name                                    <- map["name"]
        image                                   <- map["image"]
        descriptionShort                        <- map["description_short"]
        descriptionDetail                       <- map["description"]
        date                                    <- map["date"]
        dateFrom                                <- map["dateFrom"]
        dateTo                                  <- map["dateTo"]
        unlimited                               <- map["unlimited"]
        club                                    <- map["club"]
        code                                    <- map["code"]
    }
}

