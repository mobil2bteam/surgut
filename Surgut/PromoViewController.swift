
import UIKit
import SVProgressHUD

class PromoViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var clubLabel: UILabel!
    @IBOutlet weak var clubImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var qrCodeView: UIView!
    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var promoNameLabel: UILabel!
    @IBOutlet weak var promoShortDescriptionLabel: UILabel!
    @IBOutlet weak var promoDateLabel: UILabel!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!

    var promo: PromoModel!
    var promoId: Int!
    var clubSegue = "clubSegue"
    var isPromoForCode: Bool = false
    var code: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPromoForCode {
            loadPromoForCode(code: code)
        } else {
            loadPromo()
        }
        qrCodeView.layer.masksToBounds = false
        qrCodeView.layer.shadowColor = UIColor.black.cgColor
        qrCodeView.layer.shadowOpacity = 0.3
        qrCodeView.layer.shadowOffset = CGSize(width: -1, height: -1)
        qrCodeView.layer.shadowRadius = 2
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Мой Сургут"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    func loadPromo() {
        SVProgressHUD.show()
        NetworkManager.getPromo(for: promoId, successHandler: { (promo, error) in
            SVProgressHUD.dismiss()
            if promo != nil {
                self.promo = promo
                self.prepareView()
                self.errorView.isHidden = true
            }
            if error != nil {
                self.errorLabel.text = error
            }
        })
    }
    
    func loadPromoForCode(code: String) {
        SVProgressHUD.show()
        NetworkManager.checkPromo(for: code) { (promo, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self.errorLabel.text = error
            }
            if promo != nil {
                self.promo = promo
                self.prepareView()
                self.errorView.isHidden = true
            }
        }
    }
    
    func prepareView() {
        generateQRCode()
        if promo.unlimited == true {
            promoDateLabel.text = "Акция действует бессрочно"
        } else {
            promoDateLabel.text = "Акция действует до " + promo.dateTo
        }
        promoNameLabel.text = promo.name
        promoShortDescriptionLabel.text = promo.descriptionShort
        descriptionLabel.attributedText = promo.descriptionDetail.convertHtml()
        descriptionLabel.font = UIFont.init(name: "Comfortaa-Regular", size: 16)!

        let club = promo.club
        workLabel.text = "Открыт до: " + club!.work.hoursFinish
        if club!.work.freeDay == true {
            workLabel.text = "Выходной"
        }
        clubLabel.text = club!.name
        addressLabel.text = club!.address
        if let image = club!.logo {
            if let url = URL(string: image.url) {
                clubImageView.af_setImage(withURL: url)
            }
        }
        if let image = promo!.image {
            if let url = URL(string: image.url) {
                promoImageView.af_setImage(withURL: url)
            }
        }
    }

    func generateQRCode() {
        let code = promo.code
        let data = code.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            if var output = filter.outputImage {
                let scaleX = self.qrImageView.frame.size.width / output.extent.size.width;
                let scaleY = self.qrImageView.frame.size.height / output.extent.size.height;
                let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
                output = output.applying(transform)
                qrImageView.image = UIImage(ciImage: output)
            }
        }
    }
    @IBAction func clubButtonClicked(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubViewController") as! ClubViewController
        vc.clubId = promo.club.id
        navigationController?.pushViewController(vc, animated: true)
    }

}
