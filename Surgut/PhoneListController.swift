
import UIKit

class PhoneListController: UIViewController {

    @IBOutlet weak var phonesTableView: UITableView!
    var phones: [PhoneModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phonesTableView.register(UINib.init(nibName: "PhoneTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }

    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension PhoneListController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhoneTableViewCell
        cell.nameLabel.text = phones[indexPath.row].name
        cell.phoneLabel.text = phones[indexPath.row].phone
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let onlyDigits = (phones[indexPath.row].phone.components(separatedBy: CharacterSet.decimalDigits.inverted)).joined(separator: "")
        let phoneURL = URL.init(string: "telprompt:\(onlyDigits)")
        if phoneURL != nil {
            UIApplication.shared.openURL(phoneURL!)
        }
    }
    
}
