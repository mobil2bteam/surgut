
import Foundation
import Alamofire

enum ServerAPI {
    case options
    case promo(promoId: Int)
    case promoCode(promoCode: String)
    case club(clubId: Int)
}


extension ServerAPI {
    
    var path: String {
        switch self {
        case .options:
            return Constants.apiBaseUrl
        case .promo:
            return Constants.apiBaseUrl + "/news/view"
        case .club:
            return Constants.apiBaseUrl + "/club"
        case .promoCode:
            return Constants.apiBaseUrl + "/news/item"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .promo:
            return .get
        case .promoCode:
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String: Any] {
        var defaultParameters = ["key": Constants.apiKey]
        switch self {
        case .promoCode(let promoCode):
            defaultParameters["code"] = promoCode
            return defaultParameters
        case .promo(let promoId):
            defaultParameters["promo_id"] = "\(promoId)"
            return defaultParameters
        case .club(let clubId):
            defaultParameters["club_id"] = "\(clubId)"
            return defaultParameters
        default:
            return defaultParameters
        }
    }
        
}
