
import ObjectMapper
import RealmSwift


class ClubModel: Mappable {
    var id: Int!
    var name: String!
    var logo: ImageModel?
    var address: String! = ""
    var office: String! = ""
    var lat: CGFloat?
    var lng: CGFloat?
    var work: WorkModel!
    var site: String = ""
    var description: String = ""
    var descriptionShort: String = ""
    var catalogId: [Int] = []
    var cityId: Int?
    var cityName: String?
    var promos: [PromoModel] = []
    var phones: [PhoneModel] = []

    var workingHours: [WorkModel] = []
    var images: [ImageModel] = []
    var vk: String = ""
    var ok: String = ""
    var twitter: String = ""
    var instagram: String = ""
    var youtube: String = ""
    var facebook: String = ""

    required init?(map: Map) {
        
    }
    
    func socialSource() -> [[String: String]] {
        var temp = [[String: String]]()
        if !facebook.isEmpty {
            temp.append(["url": facebook, "image":"icons8-facebook-528.png"])
        }
        if !vk.isEmpty {
            temp.append(["url": vk, "image":"icons8-vk.com-528.png"])
        }
        if !instagram.isEmpty {
            temp.append(["url": instagram, "image":"icons8-instagram-528.png"])
        }
        if !youtube.isEmpty {
            temp.append(["url": youtube, "image":"icons8-youtube-528.png"])
        }
        if !twitter.isEmpty {
            temp.append(["url": twitter, "image":"icons8-twitter-528.png"])
        }
        if !ok.isEmpty {
            temp.append(["url": ok, "image":"icons8-odnoklassniki-528.png"])
        }
        return temp
    }
    
    func mapping(map: Map) {
        id                          <- map["id"]
        name                        <- map["name"]
        logo                        <- map["logo"]
        address                     <- map["address"]
        office                      <- map["office"]
        lat                         <- map["lat"]
        lng                         <- map["lng"]
        work                        <- map["work"]
        description                 <- map["description"]
        descriptionShort            <- map["description_short"]
        catalogId                   <- map["catalog_id"]
        cityId                      <- map["city_id"]
        cityName                    <- map["city_name"]
        promos                      <- map["promo"]
        images                      <- map["images"]
        workingHours                <- map["working_house"]
        phones                      <- map["phones"]
        vk                          <- map["vk"]
        ok                          <- map["ok"]
        twitter                     <- map["twitter"]
        instagram                   <- map["instagram"]
        youtube                     <- map["youtube"]
        facebook                    <- map["facebook"]
        site                        <- map["site"]
    }
}

class PhoneModel: Mappable {
    var name: String!
    var phone: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name                      <- map["name"]
        phone                     <- map["phone"]
    }
}


class WorkModel: Mappable {
    var freeDay: Bool!
    var name: String!
    var hoursStart: String!
    var hoursFinish: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        freeDay                         <- map["free_day"]
        name                            <- map["name"]
        hoursStart                      <- map["hours_start"]
        hoursFinish                     <- map["hours_finish"]
    }
}


class ClubRealmModel: Object {
    dynamic var id: Int = 0
    dynamic var logo: String = ""
    dynamic var name: String = ""
    dynamic var address: String = ""
    
    convenience init(club: ClubModel) {
        self.init()
        self.id = club.id
        self.logo = club.logo?.url ?? ""
        self.name = club.name
        self.address = club.address
    }

}
