
import UIKit

class TabViewController: UIViewController {

    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var promoContentView: UIView!
    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var testView: UIView!

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.value(forKey: "start") == nil {
            UserDefaults.standard.setValue(true, forKey: "start")
            UserDefaults.standard.synchronize()
            // show onboarding
            let vc = UIStoryboard.init(name: "Onboard", bundle: nil).instantiateInitialViewController()!
            present(vc, animated: false, completion: nil)
        } else {
            testView.isHidden = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        testView.isHidden = true
    }
    
    @IBAction func mainButtonClicked(_ sender: Any) {
        mainContentView.isHidden = false
        promoContentView.isHidden = true
        infoContainerView.isHidden = true
    }
    
    @IBAction func infoButtonClicked(_ sender: Any) {
        mainContentView.isHidden = true
        promoContentView.isHidden = true
        infoContainerView.isHidden = false
    }
    
    @IBAction func promoButtonClicked(_ sender: Any) {
        mainContentView.isHidden = true
        promoContentView.isHidden = false
        infoContainerView.isHidden = true
    }
}
