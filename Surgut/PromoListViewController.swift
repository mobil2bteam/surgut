
import UIKit
import SVProgressHUD

class PromoListViewController: UIViewController {
   
    // MARK: Properties
    var promos = AppManager.shared.options.promos
    let promoCellIdentifier = String(describing: PromoCollectionViewCell.self)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Мой Сургут"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    func showPromoViewController(for promoId: Int) {
        let vc = UIStoryboard.init(name: "Promo", bundle: nil).instantiateViewController(withIdentifier: "PromoViewController") as! PromoViewController
        vc.promoId = promoId
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension PromoListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promos.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: promoCellIdentifier, for: indexPath) as! PromoCollectionViewCell
        cell.configure(for: promos[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let p = promos[indexPath.item]
        showPromoViewController(for: p.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width - 32
        return CGSize(width: width, height: 238)
    }
    
}
