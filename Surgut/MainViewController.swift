
import UIKit
import GoogleMaps
import SVProgressHUD

class MainViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var clubsCollectionView: UICollectionView!
    @IBOutlet weak var catalogCollectionView: UICollectionView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var catalogView: UIView!
    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    @IBOutlet weak var catalogViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: Properties
    let clubCellIdentifier = String(describing: ClubCollectionViewCell.self)
    let catalogCellIdentifier = String(describing: CatalogCollectionViewCell.self)
    var selectedCatalog: Int?
    var catalogs = AppManager.shared.options.catalogs[0].items
    var clubs: [ClubModel] {
        get {
            if let catalogId = selectedCatalog {
                return AppManager.shared.options.clubs.filter({ (club) -> Bool in
                    return club.catalogId.contains(catalogId)
                })
            } else {
                return AppManager.shared.options.clubs
            }
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addCamera()
        addMarkers()
        navigationController?.navigationBar.isTranslucent = true
        clubsCollectionView.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Мой Сургут"
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName: UIFont.init(name: "Comfortaa-Regular", size: 16)!]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        if catalogViewHeightConstraint.constant == 0 {
            self.navigationController?.navigationBar.barTintColor = UIColor.black
            self.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    // MARK: Google Maps
    func addCamera() {
        let camera = GMSCameraPosition.camera(withLatitude: 61.25, longitude: 73.4166, zoom: 13)
        self.mapView.camera = camera
    }
    
    func addMarkers() {
        mapView.clear()
        for c in clubs {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(c.lat!), longitude: CLLocationDegrees(c.lng!))
            marker.title = c.name
            marker.snippet = ""
            marker.setMarkerIcon(link: c.logo!.url)
            marker.map = mapView
            marker.userData = c.id
        }
    }
    
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "favorite", sender: self)
    }
    
    // MARK: Actions
    @IBAction func modeButtonClicked(_ sender: Any) {
        if mapView.isHidden {
            mapView.isHidden = false
            rightBarButton.image = UIImage.init(named: "icon_map")
            showCatalogView(show: false)
        } else {
            mapView.isHidden = true
            rightBarButton.image = UIImage.init(named: "icon_list")
            showCatalogView(show: true)
        }
    }
    
    func showCatalogView(show: Bool) {
        view.layoutIfNeeded()
        catalogViewHeightConstraint.constant = show ? 150 : 0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            if show {
                self.navigationController?.navigationBar.isTranslucent = true
            } else {
                self.navigationController?.navigationBar.barTintColor = UIColor.black
                self.navigationController?.navigationBar.isTranslucent = false
            }
        })
    }
    
    // MARK: Navigation
    func showClubViewController(for clubId: Int) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubViewController") as! ClubViewController
        vc.clubId = clubId
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clubsCollectionView {
            return clubs.count
        } else {
            return catalogs.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == clubsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: clubCellIdentifier, for: indexPath) as! ClubCollectionViewCell
            cell.configure(for: clubs[indexPath.item])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: catalogCellIdentifier, for: indexPath) as! CatalogCollectionViewCell
            let catalog = catalogs[indexPath.item]
            cell.configure(for: catalog, selectedCatalogId: selectedCatalog)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == clubsCollectionView {
            let c = clubs[indexPath.item]
            showClubViewController(for: c.id)
        } else {
            let catalog = catalogs[indexPath.item]
            if let catalogId = selectedCatalog {
                if catalogId == catalog.id {
                    selectedCatalog = nil
                } else {
                    selectedCatalog = catalog.id
                }
            } else {
                selectedCatalog = catalog.id
            }
            catalogCollectionView.reloadData()
            clubsCollectionView.reloadData()
            addMarkers()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == clubsCollectionView {
            let itemsInRow: CGFloat = 3
            let insets: CGFloat = 1 + 1 + (itemsInRow - 1)
            var width = (collectionView.frame.size.width - insets) / 3
            width = CGFloat(Int(width))
            
            return CGSize(width: width, height: width)
        } else {
            let width = (collectionView.frame.size.width - 32) / 4
            return CGSize(width: width, height: 75)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let itemsInRow: CGFloat = 3
        let insets: CGFloat = 1 + 1 + (itemsInRow - 1)
        var width = (collectionView.frame.size.width - insets) / 3
        width = CGFloat(Int(width))
        let newInset = (collectionView.frame.size.width - itemsInRow * width - 2.0) / 2.0
        return UIEdgeInsetsMake(1, newInset, 1, newInset)
    }
    
}

extension MainViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let clubId = marker.userData as! Int
        showClubViewController(for: clubId)
        return true
    }
}

extension MainViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offset = clubsCollectionView.contentOffset.y
        if decelerate { return }
        showCatalogView(show: offset < 150)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offset = clubsCollectionView.contentOffset.y
        showCatalogView(show: offset < 150)
    }
}
