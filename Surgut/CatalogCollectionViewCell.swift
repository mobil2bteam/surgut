
import UIKit
import AlamofireImage

class CatalogCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var catalogImageView: UIImageView!
    @IBOutlet weak var catalogLabel: UILabel!
    
    func configure(for catalog: CatalogItemModel, selectedCatalogId: Int? = nil) {
        catalogLabel.text = catalog.name
        catalogImageView.image = nil
        if let catalogId = selectedCatalogId {
            if catalogId == catalog.id {
                if let image = catalog.image {
                    if let url =  URL(string: image.url) {
                        catalogImageView.af_setImage(withURL: url)
                    }
                }
            } else {
                if let image = catalog.icon {
                    if let url =  URL(string: image.url) {
                        catalogImageView.af_setImage(withURL: url)
                    }
                }
            }
        } else {
            if let image = catalog.icon {
                if let url =  URL(string: image.url) {
                    catalogImageView.af_setImage(withURL: url)
                }
            }
        }
    }
}
