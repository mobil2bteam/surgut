

import UIKit
import AlamofireImage
import ImageSlideshow
import SVProgressHUD

class ClubViewController: UIViewController {
    
    
    // MARK: Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var clubLabel: UILabel!
    @IBOutlet weak var clubImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionDetailView: UIView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var promoView: UIView!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var promoCollectionView: UICollectionView!
    @IBOutlet weak var promoCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var socialViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var siteView: UIView!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var phoneButton: UIButton!

    
    // MARK: Properties
    weak var rightBarButton: UIBarButtonItem!
    var socialVC: SocialViewController? = nil
    var club: ClubModel!
    var clubId: Int!
    let promoCellIdentifier = String(describing: PromoCollectionViewCell.self)
    let observationKey = "promoCollectionView.contentSize"
    var descriptionSegue = "descriptionSegue"
    var workSegue = "workSegue"
    var socialSegue = "socialSegue"

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
        loadClub()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = ""
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black, NSFontAttributeName: UIFont.init(name: "Comfortaa-Regular", size: 16)!]
    }

    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }

    // MARK: Observation
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            let height = promoCollectionView.collectionViewLayout.collectionViewContentSize.height
            promoCollectionViewHeightConstraint.constant = height
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }

    func favoriteClicked() {
        if FavoriteManager.isClubExist(club: club!) {
            FavoriteManager.removeClub(club: club!)
            let image = UIImage.init(named: "icon_fave_empty")
            rightBarButton.image = image
        } else {
            FavoriteManager.addClub(club: club!)
            let image = UIImage.init(named: "icon_fave")
            rightBarButton.image = image
        }
    }
    
    func shareClicked() {
        if let myWebsite = URL(string: club!.site) {
            let objectsToShare = [myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }
    }

    // MARK: Methods
    func loadClub() {
        SVProgressHUD.show()
        NetworkManager.getClub(for: clubId, successHandler: { (club, error) in
            SVProgressHUD.dismiss()
            if club != nil {
                self.club = club
                self.prepareView()
                self.errorView.isHidden = true
            }
            if error != nil {
                self.errorLabel.text = error
            }
        })
    }
    
    func addBarButtons() {
        var image = UIImage.init(named: "icon_fave_empty")
        if FavoriteManager.isClubExist(club: club) {
            image = UIImage.init(named: "icon_fave")
        }
        let favorite = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(favoriteClicked))
        rightBarButton = favorite
        let share = UIBarButtonItem(image: UIImage.init(named: "icon_share"), style: .plain, target: self, action: #selector(shareClicked))
        if club!.site.isEmpty {
            navigationItem.rightBarButtonItem = favorite
        } else {
            navigationItem.rightBarButtonItems = [favorite, share]
        }
    }
    
    func prepareView() {
        prepareSliderView()
        addBarButtons()
        if club!.phones.count == 1 {
            phoneButton.setTitle(club!.phones[0].phone, for: .normal)
        }
        if club.site.isEmpty {
            siteView.isHidden = true
        }
        if club.phones.count == 0 {
            phoneView.isHidden = true
        }

        if club.socialSource().count > 0 {
            socialVC?.source = club.socialSource()
            socialVC?.socialCollectionView.reloadData()
        } else {
            socialView.isHidden = true
        }
        if club!.description.isEmpty {
            descriptionView.isHidden = true
        }
        if club!.workingHours.count == 0 {
            workView.isHidden = true
        }
        if club!.promos.count == 0 {
            promoView.isHidden = true
        }
        if club != nil {
            workLabel.text = "Открыт до: " + club!.work.hoursFinish
            if club?.work.freeDay == true {
                workLabel.text = "Выходной"
            }
            clubLabel.text = club?.name
            addressLabel.text = club?.address
        }
        if let image = club?.logo {
            if let url = URL(string: image.url) {
                clubImageView.af_setImage(withURL: url)
            }
        }
        if club.description.isEmpty {
            descriptionDetailView.isHidden = true
        }
        if club.descriptionShort.isEmpty {
            descriptionView.isHidden = true
        } else {
            descriptionLabel.attributedText = club!.descriptionShort.convertHtml()
            descriptionLabel.font = UIFont.init(name: "Comfortaa-Regular", size: 16)!
        }
        promoCollectionView.reloadData()
    }

    
    func prepareSliderView() {
        if club!.images.count > 0 {
            placeholderImageView.isHidden = true
            // Add aspect ratio to slider
            let multiplier = club.images[0].aspectRatio
            slideshow.heightAnchor.constraint(equalTo: slideshow.widthAnchor, multiplier: multiplier).isActive = true
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
            slideshow.slideshowInterval = 3
            slideshow.draggingEnabled = true
            slideshow.pageControlPosition = PageControlPosition.insideScrollView
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor.colorAccent
            slideshow.pageControl.pageIndicatorTintColor = UIColor.white
            slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
            slideshow.setImageInputs(club!.images.map({ (item) -> AlamofireSource in
                return AlamofireSource(urlString: item.url)!
            }))
        } else {
            slideshow.isHidden = true
            placeholderImageView.isHidden = false
        }
    }

    
    // MARK: Actions
    @IBAction func callButtonClicked(_ sender: Any) {
        if club!.phones.count == 1 {
            let onlyDigits = (club!.phones[0].phone.components(separatedBy: CharacterSet.decimalDigits.inverted)).joined(separator: "")
            let phoneURL = URL.init(string: "telprompt:\(onlyDigits)")
            if phoneURL != nil {
                UIApplication.shared.openURL(phoneURL!)
            }
        }
        if club!.phones.count > 1 {
            let vc = PhoneListController.init(nibName: "PhoneListController", bundle: nil)
            vc.phones = club!.phones
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func showOnMapButtonClicked(_ sender: Any) {
        let vc = MapViewController.init(nibName: "MapViewController", bundle: nil)
        vc.club = club
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func siteButtonClicked(_ sender: Any) {
        openURL(urlString: club.site)
    }

    // MARK: Navigation
    func showPromoViewController(for promoId: Int) {
        let vc = UIStoryboard.init(name: "Promo", bundle: nil).instantiateViewController(withIdentifier: "PromoViewController") as! PromoViewController
        vc.promoId = promoId
        navigationController?.pushViewController(vc, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == descriptionSegue {
            let vc = segue.destination as! DescriptionViewController
            vc.club = club!
        }
        if segue.identifier == workSegue {
            let vc = segue.destination as! WorkingViewController
            vc.club = club!
        }
        if segue.identifier == socialSegue {
            let vc = segue.destination as! SocialViewController
            socialVC = vc
            socialVC?.changedHeightCallback = { height in
                self.socialViewHeightConstraint.constant = height
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
        }
    }

    //MARK: Helper
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

}


extension ClubViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if club != nil {
            return club!.promos.count
        } else {
            return 0
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: promoCellIdentifier, for: indexPath) as! PromoCollectionViewCell
        cell.configure(for: club!.promos[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let p = club!.promos[indexPath.item]
        showPromoViewController(for: p.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width - 32
        return CGSize(width: width, height: 238)
    }
    
}
