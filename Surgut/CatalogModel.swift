
import ObjectMapper


class CatalogItemModel: Mappable {
    var id: Int!
    var image: ImageModel?
    var icon: ImageModel?
    var name: String!
    var items: [CatalogItemModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        image                   <- map["image"]
        icon                    <- map["icon"]
        items                   <- map["items"]
    }
}

class ImageModel: Mappable {
    var url: String!
    var width: Int!
    var height: Int!
    var aspectRatio: CGFloat {
        get {
            return CGFloat(height) / CGFloat(width)
        }
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        url             <- map["url"]
        width           <- map["width"]
        height          <- map["height"]
    }
}
