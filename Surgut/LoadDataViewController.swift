import UIKit

class LoadDataViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.getOptions { [weak self] (options, error) in
            if options != nil {
                self?.performSegue(withIdentifier: "mainSegue", sender: nil)
            }
            if error != nil {
                
            }
        }
    }

}
