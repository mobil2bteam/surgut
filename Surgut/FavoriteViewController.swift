
import UIKit
import SVProgressHUD

class FavoriteViewController: UIViewController {
    
    var clubs = FavoriteManager.getFilteredClubs()
    let clubCellIdentifier = String(describing: FavoriteTableViewCell.self)
    var nextClubId: Int!
    var clubSegue = "clubSegue"
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var clubsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        clubsTableView.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        refreshControl.endRefreshing()
        clubs = FavoriteManager.getFilteredClubs()
        clubsTableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Избранное"
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black, NSFontAttributeName: UIFont.init(name: "Comfortaa-Regular", size: 16)!]
        clubs = FavoriteManager.getFilteredClubs()
        clubsTableView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == clubSegue {
            let vc = segue.destination as! ClubViewController
            vc.clubId = nextClubId
        }
    }

}

extension FavoriteViewController: UITableViewDataSource, UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: clubCellIdentifier, for: indexPath) as! FavoriteTableViewCell
        cell.configure(for: clubs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let c = clubs[indexPath.item]
        nextClubId = c.id
        self.performSegue(withIdentifier: self.clubSegue, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            FavoriteManager.removeRealmClub(club: clubs[indexPath.row])
            clubs = FavoriteManager.getFilteredClubs()
            clubsTableView.reloadData()
        }
    }
    
}
