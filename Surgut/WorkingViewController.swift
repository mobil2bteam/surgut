

import UIKit

class WorkingViewController: UIViewController {

    var club: ClubModel!
    let identifier = String(describing: WorkingTableViewCell.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        title = club.name
    }

}

extension WorkingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return club.workingHours.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! WorkingTableViewCell
        cell.configure(for: club.workingHours[indexPath.row])
        return cell
    }

}
