

import UIKit

class OnboardViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!

    @IBAction func nextButtonPressed(_ sender: Any) {
        let page = pageControl.currentPage;
        if (page < pageControl.numberOfPages - 1) {
            var frame = scrollView.frame;
            frame.origin.x = frame.size.width * CGFloat(pageControl.currentPage + 1);
            frame.origin.y = 0;
            scrollView.scrollRectToVisible(frame, animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let fractionalPage = scrollView.contentOffset.x / pageWidth
        let page = lround(Double(fractionalPage))
        pageControl.currentPage = page
        if pageControl.currentPage == pageControl.numberOfPages - 1 {
            nextButton.setTitle("Начать", for: .normal)
        } else {
            nextButton.setTitle("Далее", for: .normal)
        }
    }
    
}
