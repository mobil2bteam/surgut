
import UIKit
import QRCodeReaderViewController
import SVProgressHUD

class InfoViewController: UIViewController {
    
    var qrCodeReaderViewController: QRCodeReaderViewController {
        get {
            let reader = QRCodeReader.init(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            let qrCodeReaderViewController = QRCodeReaderViewController.init(cancelButtonTitle: "Отменить", codeReader: reader, startScanningAtLoad: true, showSwitchCameraButton: true, showTorchButton: true)
            qrCodeReaderViewController.modalPresentationStyle = .formSheet;
            qrCodeReaderViewController.delegate = self;
            return qrCodeReaderViewController
        }
    }
    
    
    func showPromo(for code: String) {
        let vc = UIStoryboard.init(name: "Promo", bundle: nil).instantiateViewController(withIdentifier: "PromoViewController") as! PromoViewController
        vc.code = code
        vc.isPromoForCode = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func siteButtonClicked(_ sender: Any) {
        openURL(urlString: "http://www.mobil2b.com")
    }
    
    @IBAction func phoneButtonClicked(_ sender: Any) {
        let onlyDigits = ("8 (800) 500-55-55".components(separatedBy: CharacterSet.decimalDigits.inverted)).joined(separator: "")
        let phoneURL = URL.init(string: "telprompt:\(onlyDigits)")
        if phoneURL != nil {
            UIApplication.shared.openURL(phoneURL!)
        }
    }
    
    @IBAction func detailButtonClicked(_ sender: Any) {
        openURL(urlString: "https://m.youtube.com/watch?feature=youtu.be&v=FlnSoMzM0ak")
    }
    
    @IBAction func scanButtonClicked(_ sender: Any) {
        if (QRCodeReader.isAvailable()){
            present(qrCodeReaderViewController, animated: true, completion: nil)
        } else {
            alert(message: "Ваше устройство не поддерживает сканирование QR-кода")
        }
    }
    
    //MARK: Helper
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

}

extension InfoViewController: QRCodeReaderDelegate {
    
    func readerDidCancel(_ reader: QRCodeReaderViewController!) {
        dismiss(animated: true, completion: nil)
    }
    
    func reader(_ reader: QRCodeReaderViewController!, didScanResult result: String!) {
        dismiss(animated: true) { 
            self.showPromo(for: result ?? "")
        }
    }
}
