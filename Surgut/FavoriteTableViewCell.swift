
import UIKit
import AlamofireImage

class FavoriteTableViewCell: UITableViewCell {
   
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.3
        shadowView.layer.shadowOffset = CGSize(width: -1, height: -1)
        shadowView.layer.shadowRadius = 2
    }

    func configure(for club: ClubRealmModel) {
        nameLabel.text = club.name
        addressLabel.text = club.address
        if let url =  URL(string: club.logo) {
            logoImageView.af_setImage(withURL: url)
        } else {
            logoImageView.image = nil
        }
    }

}
