
import UIKit

class WorkingTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(for work: WorkModel) {
        dayLabel.text = work.name
        if work.freeDay == true {
            hoursLabel.text = "Выходной"
        } else {
            hoursLabel.text = work.hoursStart + " - " + work.hoursFinish
        }
    }

}
