
import Alamofire
import ObjectMapper

class NetworkManager {
    
    static let appManager = AppManager.shared
    
    class func getOptions(successHandler: @escaping (OptionsModel?, String?) -> Void) {
        let r = ServerAPI.options
        
        if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as! [String : String]) {
            let options = Mapper<OptionsModel>().map(JSON: existCache.response.convertToDictionary()!)
            appManager.options = options
            successHandler(options, nil)
            return
        }

        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if (JSON?["options"] as? [String: Any]) != nil {
                        let model = Mapper<OptionsModel>().map(JSON: JSON!)!
                        appManager.options = model
                        successHandler(model, nil)
                        // Cache options
                        if let cacheTime = model.cache {
                            let cacheResponse = CacheRealmModel(url: r.path, response: JSON!, seconds: cacheTime, parameters: r.parameters as! Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }

                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    class func getPromo(for promoId: Int, successHandler: @escaping (PromoModel?, String?) -> Void) {
        let r = ServerAPI.promo(promoId: promoId)
        
        if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as? [String : String]) {
            let promo = Mapper<PromoModel>().map(JSON: existCache.response.convertToDictionary()!)
            successHandler(promo, nil)
            return
        }

        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let promoResponse = JSON?["promo"] as? [String: Any] {
                        let model = Mapper<PromoModel>().map(JSON: promoResponse)!
                        successHandler(model, nil)
                        
                        // Cache
                        if let cacheTime = JSON?["cache"] as? Int {
                            let cacheResponse = CacheRealmModel(url: r.path, response: promoResponse, seconds: cacheTime, parameters: r.parameters as? Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    
    class func checkPromo(for promoCode: String, successHandler: @escaping (PromoModel?, String?) -> Void) {
        let r = ServerAPI.promoCode(promoCode: promoCode)
        print(r.parameters)
        if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as? [String : String]) {
            let promo = Mapper<PromoModel>().map(JSON: existCache.response.convertToDictionary()!)
            successHandler(promo, nil)
            return
        }
        
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let promoResponse = JSON?["promo"] as? [String: Any] {
                        let model = Mapper<PromoModel>().map(JSON: promoResponse)!
                        successHandler(model, nil)
                        
                        // Cache
                        if let cacheTime = JSON?["cache"] as? Int {
                            let cacheResponse = CacheRealmModel(url: r.path, response: promoResponse, seconds: cacheTime, parameters: r.parameters as? Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    class func getClub(for clubId: Int, successHandler: @escaping (ClubModel?, String?) -> Void) {
        let r = ServerAPI.club(clubId: clubId)
        
        if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as? [String : String]) {
            let club = Mapper<ClubModel>().map(JSON: existCache.response.convertToDictionary()!)
            successHandler(club, nil)
            return
        }

        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let clubResponse = JSON?["club"] as? [String: Any] {
                        let model = Mapper<ClubModel>().map(JSON: clubResponse)!
                        successHandler(model, nil)
                        
                        if let cacheTime = JSON?["cache"] as? Int {
                            let cacheResponse = CacheRealmModel(url: r.path, response: clubResponse, seconds: cacheTime, parameters: r.parameters as? Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }

                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

}



