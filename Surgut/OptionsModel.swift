
import ObjectMapper

class OptionsModel: Mappable {
    var cache: Int?
    var urls: [UrlModel]!
    var catalogs: [CatalogItemModel] = []
    var clubs: [ClubModel] = []
    var promos: [PromoModel] = []

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        cache           <- map["cache"]
        urls            <- map["options.urls"]
        catalogs        <- map["catalog"]
        clubs           <- map["clubs.list"]
        promos          <- map["promo"]
    }
}

class UrlModel: Mappable {
    var name: String!
    var url: String!
    var method: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name        <- map["name"]
        url         <- map["url"]
        method      <- map["method"]
    }
}
