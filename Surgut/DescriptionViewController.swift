
import UIKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    var club: ClubModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = club.name
        descriptionLabel.attributedText = club!.description.convertHtml()
        descriptionLabel.font = UIFont.init(name: "Comfortaa-Regular", size: 16)!
    }

}
