
import UIKit
import AlamofireImage

class ClubCollectionViewCell: UICollectionViewCell {
    
    
    // MARK: Outlets
    @IBOutlet weak var clubImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(for club:ClubModel) {
        guard let image = club.logo,
            let url =  URL(string: image.url) else {
                clubImageView.image = nil
                return
        }
        clubImageView.af_setImage(withURL: url)
    }
    
}
